## Run project with:

``` 
npm run start:hn
```

Force build

``` 
npm run start:build
```
Load initial data:
http://localhost:3000/news/loadAll

Url app:
http://localhost:4200

## Aditional

Clean news:
http://localhost:3000/news/removeAll

Used ports: [4200, 27017, 3000]