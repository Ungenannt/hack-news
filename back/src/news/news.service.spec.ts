import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from './news.service';
import { Model, model } from 'mongoose';
import { News, NewsSchema, NewsDocument } from './schemas/news.schema';
import { getModelToken, Schema } from '@nestjs/mongoose';
import { HttpService } from '@nestjs/common';
import { NewsModule } from './news.module';

describe('NewsService', () => {
  let service: NewsService;
  const mockRepository = () => {
    find: jest.fn().mockReturnValue([{
      _id: '5dbff32e367a343830cd2f49',
      id: '1',
      title: 'title',
      author: 'author',
      url: 'link',
      date: 'date',
      deleted: false,
      _v: 0,
    },{
      _id: '5dbff89209dee20b18091ec3',
      id: '2',
      title: 'title 2',
      author: 'author 2',
      url: 'link 2',
      date: 'date 2',
      deleted: false,
      _v: 0,
    },
    ]);
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [NewsModule],
      providers: [NewsService, { provide: model(News.name, NewsSchema), useFactory: mockRepository }],
    }).compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('get news', () => {
    const newsModel = model<NewsDocument>(getModelToken(News.name), NewsSchema);
    const httpService = new HttpService();
    const service = new NewsService(newsModel, httpService);
    beforeAll(() => {
      service.findAll = jest.fn().mockResolvedValue([
        {
          _id: '5dbff32e367a343830cd2f49',
          id: '1',
          title: 'title',
          author: 'author',
          url: 'link',
          date: 'date',
          deleted: false,
          _v: 0,
        },{
          _id: '5dbff89209dee20b18091ec3',
          id: '2',
          title: 'title 2',
          author: 'author 2',
          url: 'link 2',
          date: 'date 2',
          deleted: false,
          _v: 0,
        }
      ])
    });
    it('shoul read all actives', async () => {
      const expected = [new News(
        {
          _id: '5dbff32e367a343830cd2f49',
          id: '1',
          title: 'title',
          author: 'author',
          url: 'link',
          date: 'date',
          deleted: false,
          _v: 0,
        }), new News({
          _id: '5dbff89209dee20b18091ec3',
          id: '2',
          title: 'title 2',
          author: 'author 2',
          url: 'link 2',
          date: 'date 2',
          deleted: false,
          _v: 0,
        })
      ];
      await expect(service.findAll()).resolves.toEqual(expected);
    })
  });
});
